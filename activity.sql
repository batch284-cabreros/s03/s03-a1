# Adding records to the blog_db database:

USE blog_db;


# ======INSTRUCTION 1============ INSERT INTO USERS
INSERT INTO users (email, password, datetime_created, user_id) VALUES ("johnsmith@gmail.com", "passwordA", "2121-01-01 01:00:00", 1);

INSERT INTO users (email, password, datetime_created, user_id) VALUES ("juandelacruz@gmail.com", "passwordB", "2121-01-01 02:00:00", 2);

INSERT INTO users (email, password, datetime_created, user_id) VALUES ("janesmith@gmail.com", "passwordC", "2121-01-01 03:00:00", 3);

INSERT INTO users (email, password, datetime_created, user_id) VALUES ("mariadelacruz@gmail.com", "passwordD", "2121-01-01 04:00:00", 4);

INSERT INTO users (email, password, datetime_created, user_id) VALUES ("johndoe@gmail.com", "passwordE", "2121-01-01 05:00:00", 5);


# =====INSTRUCTION 2========= INSERT INTO POSTS
INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("First Code", "Hello World!", "2121-01-01 01:00:00", 1);

INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("Second Code", "Hello Earth!", "2121-01-01 02:00:00", 2);

INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("Third Code", "Welcome to Mars!", "2121-01-01 03:00:00", 3);

INSERT INTO posts (title, content, datetime_posted, author_id) VALUES ("Fourth Code", "Bye bye solar system!", "2121-01-01 04:00:00", 4);


#=====INSTRUCTION 3==========MODIFY RECORDS

-- Get all the post with an Author ID of 1.
USE blog_db;

SELECT * FROM posts WHERE author_id = 1;

-- Get all user's email and datetime of creation.
USE blog_db;

SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the Earth!", where its initial content is "Hello Earth!" by using the record's ID.
USE blog_db;

UPDATE posts SET content = 'Hello to the people of the Earth!' WHERE author_id = 2;


-- Delete the user with an email of "johndoe@gmail.com"
USE blog_db;

DELETE FROM users WHERE email = 'johndoe@gmail.com';
